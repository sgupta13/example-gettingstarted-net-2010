﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Silanis.ESL.SDK;
using Silanis.ESL.SDK.Builder;
using System.IO;

namespace ConsoleApplication1
{
    class Program
    {
        public static readonly String API_URL = "https://sandbox.e-signlive.com/api"; // USE https://apps.e-signlive.com/api FOR PRODUCTION
        public static void Main(string[] args)
        {
            String API_KEY = Properties.Resources.API_KEY;
            EslClient eslClient = new EslClient(API_KEY, API_URL);

            Stream file = new MemoryStream(Properties.Resources.test);

            DocumentPackage superDuperPackage = PackageBuilder.NewPackageNamed("GettingStartedExample: " + DateTime.Now)
                .WithSettings(DocumentPackageSettingsBuilder.NewDocumentPackageSettings())
                    .WithSigner(SignerBuilder.NewSignerWithEmail("johnsmith@mailinator.com")
                                .WithFirstName("John")
                                .WithLastName("Smith")
                                .WithCustomId("SIGNER1"))
                    .WithDocument(DocumentBuilder.NewDocumentNamed("Test document")
                                  .FromStream(file, DocumentType.PDF)
                                  .EnableExtraction()
                                  .WithSignature(SignatureBuilder.InitialsFor("johnsmith@mailinator.com")
                                  .AtPosition(200,400)))
                                  .Build();

            PackageId packageId = eslClient.CreatePackage(superDuperPackage);
            eslClient.SendPackage(packageId); 
        }
    }
}
